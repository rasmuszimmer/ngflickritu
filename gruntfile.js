/**
 * Created by RasmusZimmer on 21-04-2015.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['app/app.js', 'app/components/**/*.js'],
                // the location of the resulting JS file
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jasmine : {
            src : ['app/app.js', 'app/components/**/*.js'],
            options: {
                specs: 'app/**/*test.js',
                vendor: [
                    "app/bower_components/angular/angular.js",
                    "app/bower_components/angular-mocks/angular-mocks.js",
                    "app/bower_components/angular-route/angular-route.js",
                    "app/bower_components/jquery/dist/jquery.js",
                    "app/bower_components/bootstrap/dist/js/bootstrap.js",
                    "app/bower_components/underscore/underscore.js"
                ]
            }
        },
        jshint: {
            // define the files to lint
            files: ['gruntfile.js', 'app/components/**/*.js', 'server.js', 'app/app.js'],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    inject: true,
                    describe: true,
                    beforeEach: true,
                    it: true,
                    expect: true,
                    angular: true,
                    sessionStorage: true,
                    localStorage: true,
                    jasmine: true
                }
            }
        },
        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'jasmine']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');


    grunt.registerTask('test', ['jshint', 'jasmine']);

    grunt.registerTask('default', ['jshint', 'jasmine', 'concat', 'uglify']);

};