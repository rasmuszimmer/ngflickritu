/**
 * Created by RasmusZimmer on 14-04-2015.
 */
var express = require('express');
var app = express();
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var Flickr = require("flickrapi");

app.use(express.static('app'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

var FlickrOptions = {
    api_key: "db4e9ec71e2cc62070b8f7ddc3f51332",
    secret: "536f66804734666b"
};

Flickr.tokenOnly(FlickrOptions, function(error, flickr) {
    // we can now use "flickr" as our API object,
    // but we can only call public methods and access public data
    app.get('/', function (req, res) {
        res.sendFile('index.html');
    });

    app.post('/flickr/search', function (req, res) {
        if(req.body.query.text){
            flickr.photos.search(req.body.query, function(err, result) {
                if(err) {
                    throw new Error(err);
                }
                res.json(result);
            });
        }else{
            res.end("no query received");
        }

    });

    app.listen(process.env.PORT || 3000);
});



function authenticator(req, res, next) {
    // assuming your session management uses req.session:
    if(req.session.authenticated) {
        return next();
    }
    next({status:403, message: "not authorised to call API methods"});
}