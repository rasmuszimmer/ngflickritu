(function() {



'use strict';

// Declare app level module which depends on views, and components
angular.module('ngFlickrITU', [
    'ngRoute',
    'ngFlickrITU.findPhotos',
    'ngFlickrITU.myPhotos',
    'ngFlickrITU.version'
]).
    run(function ($rootScope) {
        $rootScope.canvasInput = null;
        $rootScope.currentPage = 1;
        $rootScope.isLoading = true;
        $rootScope.shiftPressed = false;
        $(document).keydown(function (e) {
            if (e.keyCode == 16) {
                $rootScope.shiftPressed = true;
            }
        });
        $(document).keyup(function (e) {
            if (e.keyCode == 16) {
                $rootScope.shiftPressed = false;
            }
        });

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if(!(current && current.$$route)){
                return;
            }
            if(current.$$route.originalPath == "/my-photos"){
                $rootScope.$broadcast('savestate');
            }

            if(!(next && next.$$route)){
                return;
            }

            if(next.$$route.originalPath == "/my-photos"){
                $rootScope.$broadcast('restorestate');
            }
        });

    })

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/find-photos'});
    }])

    .directive('drawable', ['$rootScope', function ($rootScope) {
        return function (scope, element, attrs) {
            var canvas = $(element),
                containerElem = canvas.parent(),
                ctx = canvas[0].getContext("2d");

            // Set up canvas
            canvas[0].width = canvas.parent().width();

            canvas.dblclick(function (e, ui) {
                if (canvas.prop("tagName") === "CANVAS") {

                    if($rootScope.canvasInput){
                        $rootScope.canvasInput.remove();
                    }
                    // Create an input field for specifying a text to be drawn
                    $rootScope.canvasInput = $("<div class='annotation'>" +
                    "<input placeholder='Text to draw' class='form-control' type='text' autofocus='autofocus'>" +
                    "<input placeholder='Format (e.g: 10px Arial)' class='form-control' type='text'></div>");
                    containerElem.append($rootScope.canvasInput);
                    var inputElem = $rootScope.canvasInput.find("input").first(),
                        sizeElem = $rootScope.canvasInput.find("input").last();
                    $rootScope.canvasInput.position({my: "center", at: "center", of: e});
                    inputElem.focus();

                    // If Enter is pressed while focusing the input field
                    $rootScope.canvasInput.keyup(function(e) {
                      if(e.which === 13){

                        // Draw the text
                        var x = $rootScope.canvasInput.position().left - canvas[0].offsetLeft,
                            y = $rootScope.canvasInput.position().top - canvas[0].offsetTop;
                          ctx.font = $(sizeElem).val() || "50px Sprinklescolors";
                          ctx.fillText($(inputElem).val(),x,y);
                          $rootScope.canvasInput.remove();
                      }
                    });
                }else{
                    throw new Error("element should be canvas");
                }
            });

            $rootScope.clearAnnotations = function() {
                ctx.clearRect(0, 0, canvas[0].width, canvas[0].height);
            };
        };
    }])

    .directive('droppable', function () {
        return function (scope, element, attrs) {
            var elem = $(element);
            elem.droppable({
                drop: scope.onDrop,
                hoverClass: "hover",
                activeClass: "active",
                tolerance: "touch"
            });
        };
    })

    .directive('draggable', ['$rootScope', function ($rootScope) {
        return function (scope, element, attrs) {
            var elem = $(element),
                dragThreshold = $("#dLabel").offset().left;
            elem.draggable({
                containment: "div[ng-view]",
                refreshPositions: true,
                addClasses: true,
                start: function (e, ui) {

                },
                stop: function () {
                    $("aside.sidebar").fadeOut();
                },
                drag: function (e, ui) {
                    $("aside.sidebar").show();
                    var rightEdgePos = ui.offset.left + $(ui.helper).width();
                    if (rightEdgePos > dragThreshold) {
                        $("aside.sidebar").addClass("active");
                    } else {
                        $("aside.sidebar").removeClass("active");
                    }
                    if ($rootScope.shiftPressed) {
                        var rotateCSS = 'rotate(' + ui.position.left + 'deg)';
                        $(this).css({
                            '-moz-transform': rotateCSS,
                            '-webkit-transform': rotateCSS
                        });
                    }
                }
            });
        };
    }])

    .directive('repeatPostProcess', function () {
        return function (scope, element, attrs) {
            var elem = $(element),
                img = elem.find("img"),
                text = elem.find(".img-title");

            elem.attr("style", scope.photo.style);
            $(img).bind("load", function () {
                text.css("width", img.css("width"));
            });
        };
    })

    .service('flickrPhotos', ['localStorageService', function (localStorageService) {
        var photos = [],
            selectedPhotos = [],
            myPhotos = localStorageService.getMyPhotos();

        var showImgText = true;
        var imageSize = {name: "m"};

        var addPhotos = function (newPhotos) {
            photos.splice(0, photos.length);
            selectedPhotos.splice(0, selectedPhotos.length);
            var mappedPhotos = _.compact(_.map(newPhotos, function (newPhoto) {
                var obj = {
                    title: newPhoto.title,
                    url: "https://farm" + newPhoto.farm + ".staticflickr.com/" + newPhoto.server +
                        "/" + newPhoto.id + "_" + newPhoto.secret + "_" + imageSize.name + ".jpg"
                };
                if (!_.findWhere(myPhotos, {title: obj.title, url: obj.url})) {
                    return obj;
                }
            }));
            photos.push.apply(photos, mappedPhotos);
        };

        var removePhotoFromList = function (list, photo) {
            var index = _.indexOf(list, photo);
            if (index !== -1)
                list.splice(index, 1);
        };

        var addToMyPhotos = function () {

            //Animate moving from photos to myPhotos
            _.each(selectedPhotos, function (photo) {
                var photoElem = $('img[src="' + photo.url + '"]').parent();
                photoElem.position({
                    my: "center center",
                    at: "top left",
                    of: ".photo-control-bar img",
                    using: function (pos) {
                        $(this).animate({
                            top: pos.top,
                            left: pos.left,
                            'z-index': 1200
                        }, {duration: 'fast', queue: false}).fadeOut(700, function () {
                            removePhotoFromList(photos, photo);
                        });
                    }
                });
            });

            myPhotos.push.apply(myPhotos, selectedPhotos);
            selectedPhotos.splice(0, selectedPhotos.length);
        };

        var clearMyPhotos = function () {
            localStorageService.clearMyPhotos();
        };

        var getPhotos = function () {
            return photos;
        };

        var getSelectedPhotos = function () {
            return selectedPhotos;
        };

        var toggleShowImgText = function () {
            showImgText = !showImgText;
        };

        var getShowImgText = function () {
            return showImgText;
        };

        var getImageSize = function () {
            return imageSize;
        };

        var setImageSize = function (sizeName) {
            imageSize = sizeName;
        };

        return {
            getPhotos: getPhotos,
            addPhotos: addPhotos,
            getSelectedPhotos: getSelectedPhotos,
            clearMyPhotos: clearMyPhotos,
            addToMyPhotos: addToMyPhotos,
            removePhotoFromList: removePhotoFromList,
            toggleShowImgText: toggleShowImgText,
            getShowImgText: getShowImgText,
            getImageSize: getImageSize,
            setImageSize: setImageSize
        };
    }])

    .factory('localStorageService', ['$rootScope', function ($rootScope) {
        var service = {

            model: angular.fromJson(localStorage.getItem("localStorageService")) || {myPhotos: []},

            clearMyPhotos: function() {
                service.model.myPhotos.splice(0, service.model.myPhotos.length);
            },

            getMyPhotos: function() {
                return service.model.myPhotos;
            },

            SaveState: function () {
                saveState();
            },

            RestoreState: function () {
                //debugger;
                //var savedPhotos = angular.fromJson(localStorage.getItem("localStorageService")).myPhotos;
                //if(!savedPhotos){
                //
                //}
                //service.model.myPhotos.splice(0, service.model.myPhotos.length);
                //service.model.myPhotos.push.apply(service.model.myPhotos, angular.fromJson(localStorage.getItem("localStorageService")).myPhotos);
                ////service.model = angular.fromJson(localStorage.getItem("localStorageService")).myPhotos;
            }

        };

        function saveState() {
            // Find corresponding DOM element
            _.each(service.model.myPhotos, function(photo) {
                var elem = _.find($("figure.flickr-img-container"), function(elem) {
                    return $.data(elem).$scope.photo == photo;
                });
                photo.style = $(elem).attr("style");
            });
            localStorage.setItem("localStorageService", angular.toJson(service.model))
        }

        $rootScope.$on("savestate", service.SaveState);
        $rootScope.$on("restorestate", service.RestoreState);

        return service;
    }])

    .controller('NavbarCtrl', ['$rootScope', '$scope', '$location', '$http', 'flickrPhotos', 'localStorageService',
        function ($rootScope, $scope, $location, $http, flickrPhotos, localStorageService) {

        $scope.bucketSizeMyPhotos = 0;
        $scope.$watch(function () {
            return localStorageService.getMyPhotos().length;
        }, function (item) {
            $scope.bucketSizeMyPhotos = item;
        }, true);

        $scope.bucketSize = 0;
        $scope.$watch(function () {
            return flickrPhotos.getSelectedPhotos().length;
        }, function (item) {
            $scope.bucketSize = item;
        }, true);

        $scope.imageSizes = [
            {name: "s", text: "small square 75x75", tip: "square 75x75"},
            {name: "q", text: "large square 150x150", tip: "square 150x150"},
            {name: "t", text: "thumbnail, 100 on longest side", tip: "100 longest"},
            {name: "m", text: "small, 240 on longest side", tip: "240 longest"},
            {name: "n", text: "small, 320 on longest side", tip: "320 longest"},
            {name: "z", text: "medium 640, 640 on longest side", tip: "640 longest"},
            {name: "c", text: "medium 800, 800 on longest side", tip: "800 longest"},
            {name: "b", text: "large, 1024 on longest side", tip: "1024 longest"}
        ];


        $scope.imageSize = _.findWhere($scope.imageSizes, {name: flickrPhotos.getImageSize().name});
        $scope.changeImgSize = function (size) {
            flickrPhotos.setImageSize(size);
            $scope.imageSize = size;

            if ($scope.queryString) {
                $scope.search();
            }
        };

        $scope.sizeIsActive = function (size) {
            return $scope.imageSize === size;
        };

        $scope.onAddToMyPhotosClicked = function () {
            flickrPhotos.addToMyPhotos();
        };

        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };

        $scope.toggleImgText = flickrPhotos.toggleShowImgText;
        $scope.showImgText = flickrPhotos.getShowImgText();
        $scope.$watch(function () {
            return flickrPhotos.getShowImgText();
        }, function (item) {
            $scope.showImgText = item;
        }, true);
        $scope.toggleImgTextOptText = function () {
            var end = "image text";
            return $scope.showImgText ? "Disable " + end : "Enable " + end;
        };

        $scope.bgIsActive = function (bgNum) {
            var bgUrlStr = $(".container-fluid.ng-scope").css("background-image");

            return bgUrlStr && bgUrlStr.search("scrap" + bgNum + ".jpg") !== -1;
        };

        $scope.clearMyPhotos = function () {
            flickrPhotos.clearMyPhotos();
        };

        $scope.clearAnnotations = function() {
          $rootScope.clearAnnotations();
        };

        $scope.changeBG = function (num) {
            $(".container-fluid.ng-scope").css("background-image", "url(images/scrap" + num + ".jpg)");
        };

        $scope.currentPage = $rootScope.currentPage;
        $scope.$watch(function () {
            return $rootScope.currentPage;
        }, function (item) {
            $scope.currentPage = item;
            if ($scope.latestQueryStr){
                $scope.search();
            }
        }, true);

        $scope.latestQueryStr = "";

        $scope.isLoading = false;

        $scope.queryString = "";
        $scope.onSearchClicked = function() {
            $scope.latestQueryStr = "";
            $rootScope.currentPage = 1;
            $scope.search();
        }
        $scope.search = function () {
            if(!$scope.queryString && !$scope.latestQueryStr){
                return;
            }

            $location.path('/find-photos');
            $scope.isLoading = true;
            $rootScope.isLoading = true;
            $http.post('/flickr/search', {
                query: {
                    text: $scope.queryString || $scope.latestQueryStr,
                    page: $rootScope.currentPage,
                    per_page: 50
                }
            }).success(function (data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                if($scope.queryString){
                    $scope.latestQueryStr = $scope.queryString;
                }
                $rootScope.totalPages = data.photos.pages;
                flickrPhotos.addPhotos(data.photos.photo);
                //debugger;
            }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }).finally(function () {
                    $scope.isLoading = false;
                    $rootScope.isLoading = false;
                });
        };
    }]);

})();
