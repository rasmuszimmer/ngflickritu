(function() {

    'use strict';

    angular.module('ngFlickrITU.version', [
        'ngFlickrITU.version.interpolate-filter',
        'ngFlickrITU.version.version-directive'
    ])

    .value('version', '0.1.0');

})();