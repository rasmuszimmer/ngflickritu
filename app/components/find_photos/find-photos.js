(function() {

    'use strict';

    angular.module('ngFlickrITU.findPhotos', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/find-photos', {
        templateUrl: 'components/find_photos/find-photos.html',
        controller: 'findPhotosCtrl'
      });
    }])

        .controller('findPhotosCtrl', ['$scope', 'flickrPhotos', '$rootScope',function($scope, flickrPhotos, $rootScope) {
            $scope.photos = flickrPhotos.getPhotos();
            $scope.selectedPhotos = flickrPhotos.getSelectedPhotos();
            $scope.showImgText = flickrPhotos.getShowImgText();
            $scope.$watch(function() {
                return flickrPhotos.getShowImgText();
            }, function(val) {
                $scope.showImgText = val;
            }, true);

            $scope.isSelected = function(photo) {
                return _.contains($scope.selectedPhotos, photo);
            };
            $scope.onPhotoClicked = function(photo) {
                if($scope.isSelected(photo)){
                    $scope.deselectPhoto(photo);
                }else{
                    $scope.selectPhoto(photo);
                }
            };

            $scope.prevPage = function() {
                if($rootScope.currentPage === 1){
                    return;
                }
                $rootScope.currentPage--;
            };

            $scope.nextPage = function() {
                if($rootScope.currentPage === $rootScope.totalPages){
                    return;
                }
                $rootScope.currentPage++;

            };

            $scope.currentPage = $rootScope.currentPage;
            $scope.$watch(function () {
                return $rootScope.currentPage;
            }, function (item) {
                $scope.currentPage = item;
            }, true);

            $scope.totalPages = $rootScope.totalPages;
            $scope.$watch(function () {
                return $rootScope.totalPages;
            }, function (item) {
                $scope.totalPages = item;
            }, true);

            $scope.isLoading = $rootScope.isLoading;
            $scope.$watch(function () {
                return $rootScope.isLoading;
            }, function (item) {
                $scope.isLoading = item;
            }, true);

            $scope.selectPhoto = function(photo) {
                $scope.selectedPhotos.push(photo);
            };

            $scope.deselectPhoto = function(photo) {
                flickrPhotos.removePhotoFromList($scope.selectedPhotos, photo);
            };


        }]);

})();