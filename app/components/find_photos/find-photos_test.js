(function() {

    'use strict';

    describe('ngFlickrITU.findPhotos module', function () {
        var ctrl, scope, flickrPhotos;

        beforeEach(module('ngFlickrITU.findPhotos', function ($provide) {
            flickrPhotos = {};

            flickrPhotos.getPhotos = function () {
                return [];
            };
            flickrPhotos.getSelectedPhotos = function () {
                return [];
            };
            flickrPhotos.getShowImgText = jasmine.createSpy();
            flickrPhotos.removePhotoFromList = jasmine.createSpy();

            $provide.value('flickrPhotos', flickrPhotos);
        }));

        describe('findPhotos controller', function () {

            beforeEach(inject(function ($controller, $rootScope, _flickrPhotos_) {
                // The injector unwraps the underscores (_) from around the parameter names when matching
                scope = $rootScope.$new();
                flickrPhotos = _flickrPhotos_;
                //Create the controller with the new scope
                ctrl = $controller('findPhotosCtrl', {$scope: scope, flickrPhotos: flickrPhotos});
            }));

            it('should instantiate the controller....', function () {
                expect(ctrl).toBeDefined();
            });

            describe('selectPhoto', function() {
                it('should push value when called ....', function () {
                    scope.selectPhoto("photo");
                    expect(scope.selectedPhotos).toContain("photo");
                });
            });

            describe('deselectPhoto', function() {
                it('should call removeFromList on service ....', function () {
                    scope.deselectPhoto("photo");
                    expect(flickrPhotos.removePhotoFromList).toHaveBeenCalledWith(scope.selectedPhotos, "photo");
                });
            });

            describe('isSelected', function() {
                it('should return true if "photo" is selected', function () {
                    scope.selectPhoto("photo");
                    expect(scope.isSelected("photo")).toBeTruthy();
                });
            });

            describe('onPhotoClicked', function() {
                it('should select if not selected', function () {
                    scope.selectedPhotos = ["another photo"];
                    scope.onPhotoClicked("photo");
                    expect(scope.isSelected("photo")).toBeTruthy();
                    expect(flickrPhotos.removePhotoFromList).not.toHaveBeenCalled();
                });
                it('should deselect if selected', function () {
                    scope.selectedPhotos = ["photo"];
                    scope.onPhotoClicked("photo");
                    expect(flickrPhotos.removePhotoFromList).toHaveBeenCalledWith(scope.selectedPhotos, "photo");
                });
            });

        });
    });

})();