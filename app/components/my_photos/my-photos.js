
'use strict';

angular.module('ngFlickrITU.myPhotos', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/my-photos', {
    templateUrl: 'components/my_photos/my-photos.html',
    controller: 'myPhotosCtrl'

  });
}])

    .controller('myPhotosCtrl', ['$scope', 'flickrPhotos', 'localStorageService', '$rootScope',function($scope, flickrPhotos, localStorageService, $rootScope) {
        $scope.myPhotos = localStorageService.model.myPhotos;

        $scope.knowsAnnotate = sessionStorage.getItem("knowsAnnotate");
        $scope.knowsRotate = sessionStorage.getItem("knowsRotate");
        $scope.onHintClicked = function(hint) {
            sessionStorage.setItem(hint, true);
        };

        $scope.showImgText = flickrPhotos.getShowImgText();
        $scope.$watch(function() {
            return flickrPhotos.getShowImgText();
        }, function(val) {
            $scope.showImgText = val;
        }, true);

        $scope.onDrop = function(event, ui) {
            var photo = $.data(ui.draggable[0]).$scope.photo;
            flickrPhotos.removePhotoFromList(localStorageService.model.myPhotos, photo);
            $scope.$apply();
        }

        window.onbeforeunload = function (event) {
            $rootScope.$broadcast('savestate');
        };

        $scope.$on('$destroy', function() {
            window.onbeforeunload = null;
        });
    }]);
